package awele.bot.NonoBotMinMax;

import awele.bot.Bot;
import awele.bot.Protobot;
import awele.core.Board;
import awele.core.InvalidBotException;

public class NonoBotMinMax extends Bot {

    private static final int PROFONDEUR_MAX = 6;

    private double[] decision;

    public NonoBotMinMax() throws InvalidBotException {
        this.setBotName("NonoBotMinMax");
        this.addAuthor("Arnaud Couderc");
        this.decision = initZero();
    }

    @Override
    public void initialize() {

    }

    @Override
    public double[] getDecision(Board board) {
        try {
            this.minmax(board, PROFONDEUR_MAX);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return decision;
    }

    @Override
    public void learn() {

    }


    private synchronized void minmax(Board board, int profondeur) throws InterruptedException {
        double max_val = -Double.MAX_VALUE;
        this.decision = initZero();

        double val;

        MinMaxThread thread1 = new MinMaxThread(board, profondeur, 0);
        MinMaxThread thread2 = new MinMaxThread((Board) board.clone(), profondeur, 3);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        val = thread1.getMaxVal();

        if(val > max_val || ((val == max_val) && ((Math.random() * 10) % 2 == 0)))
        {
            max_val = val;
            for(int i=0; i < 3; i++)
            {
                this.decision[i] = thread1.getDecision(i);
            }
        }

        val = thread2.getMaxVal();

        if(val > max_val || ((val == max_val) && ((Math.random() * 10) % 2 == 0)))
        {
            //max_val = val;
            for(int i=3; i < 6; i++)
            {
                this.decision[i] = thread2.getDecision(i);
            }
        }
    }

    public static double[] initZero() {
        double[] res = new double[6];
        for (int i = 0; i < 6; i++) {
            res[i] = 0;
        }
        return res;
    }

    static int[][] initZero2() {
        int[][] res = new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++)
                res[i][j] = 0;
        }
        return res;
    }
}