package awele.bot.NonoBotMinMax;

import awele.core.Board;

import static awele.bot.NonoBotMinMax.NonoBotMinMax.initZero;

public class MinMaxThread extends Thread {

    private final int profondeur;
    private double maxVal;

    private int decalage;

    private Board board;
    private double[] decision;

    double getMaxVal() {
        return maxVal;
    }

    double getDecision(int i) {
        return decision[i];
    }

    MinMaxThread(Board board, int profondeur, int decalage) {
        this.board = board;
        this.decalage = decalage;
        this.profondeur = profondeur;
        this.maxVal = -Double.MAX_VALUE;
        this.decision = initZero();
    }

    @Override
    public void run() {
        int val;
        boolean[] coupsPossibles = board.validMoves(board.getCurrentPlayer());
        for (int i = this.decalage; i < 3+this.decalage; i++) {
            if (coupsPossibles[i]) {
                double[] tmpDecision = initZero();
                tmpDecision[i] = 1;
                Board boardCopy = board.playMoveSimulationBoard(board.getCurrentPlayer(), tmpDecision);

                val = this.max(boardCopy, profondeur);

                if (val > this.maxVal || ((val == this.maxVal) && ((Math.random() * 10) % 2 == 0))) {
                    this.decision = tmpDecision;
                    this.maxVal = val;
                }
            }
        }
    }

    private int min(Board board, int profondeur) {
        int val;

        if (profondeur == 0 || board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6) {
            return eval(board);
        }

        int min_val = -Integer.MAX_VALUE;

        boolean[] coupsPossibles = board.validMoves(board.getCurrentPlayer());
        for (int i = this.decalage; i < 3+this.decalage; i++) {
            if (coupsPossibles[i]) {
                double[] tmpDecision = initZero();
                tmpDecision[i] = 1;
                Board boardCopy = board.playMoveSimulationBoard(board.getCurrentPlayer(), tmpDecision);

                val = this.max(boardCopy, profondeur - 1);

                if (val < min_val || ((val == min_val) && ((Math.random() * 10) % 2 == 0)))
                    min_val = val;
            }
        }
        return min_val;
    }

    private int max(Board board, int profondeur) {

        int val;

        if (profondeur == 0 || board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6) {
            return eval(board);
        }

        int max_val = -Integer.MAX_VALUE;

        boolean[] coupsPossibles = board.validMoves(board.getCurrentPlayer());
        for (int i = this.decalage; i < 3+this.decalage; i++) {
            if (coupsPossibles[i]) {
                double[] tmpDecision = initZero();
                tmpDecision[i] = 1;
                Board boardCopy = board.playMoveSimulationBoard(board.getCurrentPlayer(), tmpDecision);

                val = this.min(boardCopy, profondeur - 1);

                if (val > max_val || ((val == max_val) && ((Math.random() * 10) % 2 == 0)))
                    max_val = val;
            }
        }
        return max_val;
    }

    private int eval(Board board) {

        int mene = board.getScore(board.getCurrentPlayer()) > board.getScore(Board.otherPlayer(board.getCurrentPlayer())) ? board.getScore(board.getCurrentPlayer()) : board.getScore(Board.otherPlayer(board.getCurrentPlayer()));
        int diff = mene * (board.getScore(board.getCurrentPlayer()) - board.getScore(Board.otherPlayer(board.getCurrentPlayer())));

        if (board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6) {
            if (diff > 0)
                return Integer.MAX_VALUE;
            else if (diff < 0)
                return -Integer.MAX_VALUE;
            else
                return 0;
        }
        return diff;
    }



}
