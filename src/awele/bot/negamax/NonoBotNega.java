package awele.bot.negamax;

import awele.bot.Bot;
import awele.core.Board;
import awele.core.InvalidBotException;

public class NonoBotNega extends Bot {

    private static final int PROFONDEUR_MAX = 6;

    private double[] decision;

    public NonoBotNega() throws InvalidBotException {
        this.setBotName("NonoBotNega");
        this.addAuthor("Arnaud Couderc");
        this.decision = initZero();
    }

    @Override
    public void initialize() {

    }

    @Override
    public double[] getDecision(Board board) {
        this.negamax(board, PROFONDEUR_MAX, 1);
        return decision;
    }

    @Override
    public void learn() {

    }

    private int negamax(Board board, int profondeur, int couleur)
    {
        if (profondeur == 0 || board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6)
            return couleur * eval(board);

        int val = -Integer.MAX_VALUE;

        double[] tmpDecision = initZero();
        boolean[] coupsPossibles = board.validMoves(board.getCurrentPlayer());
        for (int i = 0; i < 6; i++) {
            if (coupsPossibles[i]) {
                tmpDecision = initZero();
                tmpDecision[i] = 1;
                Board boardCopy = board.playMoveSimulationBoard(board.getCurrentPlayer(), tmpDecision);

                int newVal = -negamax(boardCopy, profondeur - 1, -couleur);

                if(newVal > val)
                {
                    val = newVal;
                    this.decision = tmpDecision;
                }
            }
        }
        return val;
    }

    private int eval(Board board) {

        int mene = board.getScore(board.getCurrentPlayer()) > board.getScore(Board.otherPlayer(board.getCurrentPlayer())) ? board.getScore(board.getCurrentPlayer()) : board.getScore(Board.otherPlayer(board.getCurrentPlayer()));
        int diff = mene * (board.getScore(board.getCurrentPlayer()) - board.getScore(Board.otherPlayer(board.getCurrentPlayer())));

        if (board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6) {
            if (diff > 0)
                return Integer.MAX_VALUE;
            else if (diff < 0)
                return -Integer.MAX_VALUE;
            else
                return 0;
        }
        return diff;
    }


    private static double[] initZero() {
        double[] res = new double[6];
        for (int i = 0; i < 6; i++) {
            res[i] = 0;
        }
        return res;
    }

    static int[][] initZero2() {
        int[][] res = new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++)
                res[i][j] = 0;
        }
        return res;
    }
}