package awele.bot.alphabeta;

import awele.bot.Bot;
import awele.core.Board;
import awele.core.InvalidBotException;

public class NonoBotAB extends Bot {

    private static final int PROFONDEUR_MAX = 6;
    private static final int INFINI = Integer.MAX_VALUE;

    private double[] decision;

    public NonoBotAB() throws InvalidBotException {
        this.setBotName("NonoBotAB");
        this.addAuthor("Arnaud Couderc");
        this.decision = initZero();
    }

    @Override
    public void initialize() {

    }

    @Override
    public double[] getDecision(Board board) {
        this.alphabeta(board, PROFONDEUR_MAX, -INFINI, INFINI);
        return decision;
    }

    @Override
    public void learn() {

    }


    private int alphabeta(Board board, int profondeur, int alpha, int beta)
    {
        if (profondeur == 0 || board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6)
            return eval(board);
        else {
            int alphaP = -INFINI, betaP = INFINI;

            if(profondeur % 2 == 1)
            {
                double[] tmpDecision = initZero();
                boolean[] coupsPossibles = board.validMoves(board.getCurrentPlayer());
                for (int i = 0; i < 6; i++) {
                    if (coupsPossibles[i]) {
                        tmpDecision = initZero();
                        tmpDecision[i] = 1;
                        Board boardCopy = board.playMoveSimulationBoard(board.getCurrentPlayer(), tmpDecision);

                        int val = this.alphabeta(boardCopy, profondeur - 1, alpha, Math.min(beta, betaP));
                        betaP = Math.min(betaP, val);

                        if(alpha >= betaP)
                        {
                            /*coupure alpha*/
                            this.decision = tmpDecision;
                            return betaP;
                        }
                    }
                }
                this.decision = tmpDecision;
                return betaP;
            }
            else
            {
                double[] tmpDecision = initZero();
                boolean[] coupsPossibles = board.validMoves(board.getCurrentPlayer());
                for (int i = 0; i < 6; i++) {
                    if (coupsPossibles[i]) {
                        tmpDecision = initZero();
                        tmpDecision[i] = 1;
                        Board boardCopy = board.playMoveSimulationBoard(board.getCurrentPlayer(), tmpDecision);

                        int val = this.alphabeta(boardCopy, profondeur - 1, Math.max(alpha, alphaP), beta);
                        alphaP = Math.max(alphaP, val);

                        if(alphaP >= beta)
                        {
                            /*coupure beta*/
                            this.decision = tmpDecision;
                            return alphaP;
                        }
                    }
                }
                this.decision = tmpDecision;
                return alphaP;
            }
        }
    }

    private int eval(Board board) {

        int mene = board.getScore(board.getCurrentPlayer()) > board.getScore(Board.otherPlayer(board.getCurrentPlayer())) ?
                board.getScore(board.getCurrentPlayer()) : board.getScore(Board.otherPlayer(board.getCurrentPlayer()));
        int diff = mene * (board.getScore(board.getCurrentPlayer()) - board.getScore(Board.otherPlayer(board.getCurrentPlayer())));

        if (board.getPlayerSeeds() == 0 || board.getOpponentSeeds() == 0 || board.getNbSeeds() <= 6) {
            if (diff > 0)
                return INFINI;
            else if (diff < 0)
                return -INFINI;
            else
                return 0;
        }
        return diff;
    }



    private static double[] initZero() {
        double[] res = new double[6];
        for (int i = 0; i < 6; i++) {
            res[i] = 0;
        }
        return res;
    }
}